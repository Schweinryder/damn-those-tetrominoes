﻿public enum Direction {
	Undefined,
	NoDirection,
	Left,
	Right,
	Up,
	Down
}
