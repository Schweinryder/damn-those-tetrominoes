﻿using UnityEngine;
using System.Collections;

public class GUIBox {

	Rect position;
	string label;
	
	public GUIBox(string label) {
		this.label = label;
	}
	
	public GUIBox(string label, Rect position) {
		this.label = label;
		this.position = position;
	}
	
	public void SetLabel(string label) {
		this.label = label;
	}
	
	public void SetPosition(Rect position) {
		this.position = position;
	}

	public void Render() {
		GUI.Box (position, label);
	}

}
