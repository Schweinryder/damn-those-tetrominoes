﻿using UnityEngine;
using System.Collections;

public class GUIButton {

	Rect position;
	string label;

	public bool isHiding { get; set; }

	public GUIButton(string label) {
		this.label = label;
	}

	public GUIButton(string label, Rect position) {
		this.label = label;
		this.position = position;
	}

	public void SetLabel(string label) {
		this.label = label;
	}

	public void SetPosition(Rect position) {
		this.position = position;
	}

	public void Init (float xPosition, float yPosition, float width, float height) {
		position = new Rect(xPosition, yPosition, width, height);
	}

	public bool IsPressed() {
		if (isHiding) {
			return false;
		}
		return GUI.Button (position, label);
	}

}
