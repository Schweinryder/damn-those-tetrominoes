﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIButtonList {

	List<ListEntry> listEntries;
	//int page;

	public GUIButtonList() {
		this.listEntries = new List<ListEntry> ();
		//this.page = 1;
	}

	public void Add(string label, string key) {
		listEntries.Add (new ListEntry (label, key));
	}

	public string GetInput() {
		int index = 0;
		foreach (ListEntry entry in listEntries) {
			entry.button.SetPosition (GUIUtils.OptionRect (++index));
			if (entry.button.IsPressed()) {
				return entry.key;
			}
		}
		return null;
	}

	private class ListEntry {
		
		public GUIButton button { get; private set; }
		public string key { get; private set; }

		public ListEntry(string label, string key) {
			this.button = new GUIButton(label);
			this.key = key;
		}
	}

}
