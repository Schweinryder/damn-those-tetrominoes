﻿using UnityEngine;
//using UnityEditor;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class GUIController : MonoBehaviour {

	GUIMenu mainMenu;

	// Use this for initialization
	void Start () {
		loadMainMenu ();
	}

	public void Reset() {
		loadMainMenu ();
	}

	private void loadMainMenu() {
		mainMenu = GUIService.GetMainMenu ();
		if (mainMenu != null) {
			mainMenu.Select (null);
		}
	}



}
