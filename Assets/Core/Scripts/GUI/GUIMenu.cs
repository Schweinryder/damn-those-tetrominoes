﻿using UnityEngine;
using System.Collections;

public abstract class GUIMenu : MonoBehaviour {

	private GUIMenu parent;
	private GUIButton backButton;
	private bool isActive;
	private CloseAction closeAction;

	void Awake () {
		parent = null;
		isActive = false;
	}

	public void Select (GUIMenu selector) {
		foreach (GUIMenu menu in FindObjectsOfType<GUIMenu> ()) {
			if (menu.isActive) {
				menu.isActive = false;
				menu.OnClosed ();
			}
		}
		if (selector != null) {
			this.parent = selector;
		}
		this.isActive = true;
		this.backButton = new GUIButton ("Back", GUIUtils.PopupToggleRect (Side.Left));
		OnSelected ();
	}

	protected virtual void OnSelected () {}
	
	void OnGUI () {
		if (isActive) {
			if (closeAction != null) {
				RenderCloseConfirmation ();
				return;
			} else {
				Render ();
				if (parent != null && backButton != null && DoRenderBackButton()) {
					if (backButton.IsPressed()) {
						Close ();
					}
				}
			}
		}
	}

	protected virtual bool DoRenderBackButton() {
		return true;
	}

	protected virtual bool DoShowCloseConfirmation() {
		return false;
	}

	protected virtual string GetCloseConfirmationString() {
		return "Are you sure?";
	}

	protected void Close () {
		Close (false);
	}

	private void Close (bool forceClose) {
		if (!DoShowCloseConfirmation () || forceClose) {
			if (parent != null) {
				parent.Select (null);
			}
			OnClosed ();
		} else {
			closeAction = new CloseAction ();
			OnConfirmationOpen ();
		}
	}

	protected virtual void OnClosed() {}

	protected abstract void Render ();

	private void RenderCloseConfirmation() {
		GUI.Box (GUIUtils.ConfirmationTextRect (), GetCloseConfirmationString());
		if (closeAction.yesButton.IsPressed()) {
			Close (true);
			closeAction = null;
		} else if (closeAction.noButton.IsPressed()) {
			closeAction = null;
		}

		if (closeAction == null) {
			OnConfirmationClose ();
		}
	}

	protected virtual void OnConfirmationOpen() {}

	protected virtual void OnConfirmationClose() {}

	private class CloseAction {

		public GUIButton yesButton { get; private set; }
		public GUIButton noButton { get; private set; }

		public CloseAction() {
			yesButton = new GUIButton("Yes", GUIUtils.RightConfirmationRect());
			noButton = new GUIButton("No", GUIUtils.LeftConfirmationRect());
		}

	}

}
