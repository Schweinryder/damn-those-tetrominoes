﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIPopupButtonList {

	private enum ViewMove {
		Hide,
		Show
	}

	ViewMove viewMode;

	GUIButton toggleButton;
	List<GUIButton> buttons;

	private GUIPopupButtonList(Rect toggleButtonPosition) {
		toggleButton = new GUIButton ("X", toggleButtonPosition);
		buttons = new List<GUIButton> ();
	}

	public static GUIPopupButtonList NewInstance (Side side) {
		return new GUIPopupButtonList(GUIUtils.PopupToggleRect (side));
	}

	public void AddButton (string label) {
		buttons.Add (new GUIButton (label, GUIUtils.OptionRect (buttons.Count)));
	}

	public void AddBottomButton(string label) {
		buttons.Add (new GUIButton (label, GUIUtils.OptionRect (GUIUtils.MAX_OPTIONS - 1)));
	}

	public bool IsShow() {
		return viewMode == ViewMove.Show;
	}

	public int GetInputValue() {
		return GetInputValue(false);
	}

	public int GetInputValue (bool ignoreInput) {
		if (doToggle(ignoreInput)) {
			toggleViewMode ();
		}

		if (viewMode == ViewMove.Show) {
			return pressedButtonIndex (ignoreInput);
		}
		return -1;
	}

	private bool doToggle(bool ignoreInput) {
		bool pressedToggle = toggleButton.IsPressed ();
		if (!ignoreInput || viewMode == ViewMove.Show) {
			return pressedToggle;
		}
		return false;
	}

	public void Show() {
		viewMode = ViewMove.Show;
	}

	public void Hide() {
		viewMode = ViewMove.Hide;
	}

	private void toggleViewMode() {
		if (viewMode == ViewMove.Hide) {
			viewMode = ViewMove.Show;
		} else if (viewMode == ViewMove.Show) {
			viewMode = ViewMove.Hide;
		}
	}

	private int pressedButtonIndex(bool ignoreInput) {
		for (int i = 0; i < buttons.Count; i++) {
			if (buttons[i].IsPressed()) {
				if (!ignoreInput || viewMode == ViewMove.Show) {
					viewMode = ViewMove.Hide;
					return i;
				}
			}
		}
		return -1;
	}

}
