﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIService : MonoBehaviour {	

	public static GUIService Instance () {
		return FindObjectOfType<GUIService> ();
	}

	const float messageShowTime = 3f;

	float errorTimer;
	string errorMessage;

	public GameObject[] menuPrefabs;
	private static List<GUIMenu> menus;

	void Awake() {
		menus = new List<GUIMenu> ();
		foreach (GameObject menuPrefab in menuPrefabs) {
			GameObject newMenuObject = GameObjectService.SpawnObject (menuPrefab);
			menus.Add (newMenuObject.GetComponent<GUIMenu> ());
		}
	}

	public void ShowErrorMessage(string message) {
		errorTimer = messageShowTime;
		errorMessage = message;
	}

	void OnGUI() {
		if (errorTimer > 0) {
			GUI.Box (GUIUtils.ErrorMessageRectWide(), errorMessage);
			errorTimer -= Time.deltaTime;
		}
	}

	public static GUIMenu GetMainMenu () {
		if (menus.Count > 0) {
			return menus [0];
		} else
			return null;
	}

	public static T Get<T>() where T: GUIMenu {
		foreach (GUIMenu menu in menus) {
			T wantedMenu = menu as T;
			if (wantedMenu != null) {
				return wantedMenu;
			}
		}
		return null;
	}
}
