﻿using UnityEngine;
using System.Collections;

public static class GUIUtils {

	public static Rect ErrorMessageRect() {
		float width = Screen.width / 4;
		float height = Screen.height / 10;
		float xPosition = (Screen.width / 2) - (width / 2f);
		float yPosition = height * 2;
		return new Rect (xPosition, yPosition, width, height);
	}

	public static Rect ErrorMessageRectWide() {
		float width = Screen.width;
		float height = Screen.height / 10;
		float xPosition = 0;
		float yPosition = height * 2;
		return new Rect (xPosition, yPosition, width, height);
	}

	public static Rect PopupToggleRect (Side side) {
		float width = Screen.width / 10f;
		float height = width;
		float top = Screen.height - height;
		float left;
		if (side == Side.Left) {
			left = 0;
		} else {
			left = Screen.width - width;
		}
		return new Rect (left, top, width, height);
	}

	public static Rect MidLeftRect() {		
		float width = Screen.width / 10f;
		float height = Screen.height / 10f;
		float left = width;
		float top = (Screen.height / 2f) - (height / 2f);
		return new Rect(left, top, width, height);
	}

	public static Rect MidRightRect() {		
		float width = Screen.width / 10f;
		float height = Screen.height / 10f;
		float left = Screen.width - (width * 2f);
		float top = (Screen.height / 2f) - (height / 2f);
		return new Rect(left, top, width, height);
	}

	public static Rect TopLeftRect() {		
		float width = Screen.width / 10f;
		float height = Screen.height / 10f;
		float left = 0;
		float top = 0;
		return new Rect(left, top, width, height);
	}

	public static Rect TopRightRect() {		
		float width = Screen.width / 10f;
		float height = Screen.height / 10f;
		float left = Screen.width - width;
		float top = 0;
		return new Rect(left, top, width, height);
	}

	public static Rect ConfirmationTextRect() {
		float width = ScreenUtils.WidthPercent (50f);
		float height = ScreenUtils.HeightPercent (30f);
		float left = (Screen.width / 2) - width / 2;
		float top = Screen.height - ScreenUtils.HeightPercent (70f);
		return new Rect(left, top, width, height);
	}

	public static Rect LeftConfirmationRect() {
		float width = ScreenUtils.WidthPercent (25f);
		float height = ScreenUtils.HeightPercent (10f);
		float left = width;
		float top = Screen.height - ScreenUtils.HeightPercent (40f);
		return new Rect(left, top, width, height);
	}

	public static Rect RightConfirmationRect () {
		float width = ScreenUtils.WidthPercent (25f);
		float height = ScreenUtils.HeightPercent (10f);
		float left = width * 2;
		float top = Screen.height - ScreenUtils.HeightPercent (40f);
		return new Rect(left, top, width, height);
	}

	public static Rect TopMidRect() {		
		float width = Screen.width / 10f;
		float height = Screen.height / 10f;
		float left = (Screen.width / 2f) - (width / 2f);
		float top = height;
		return new Rect(left, top, width, height);
	}

	public static Rect BottomMidRect() {		
		float width = Screen.width / 10f;
		float height = Screen.height / 10f;
		float left = (Screen.width / 2f) - (width / 2f);
		float top = Screen.height - (height * 2f);
		return new Rect(left, top, width, height);
	}

	public static Rect TopWideRect() {
		float height = Screen.height / 10;
		float top = height;
		return new Rect(0, top, Screen.width, height);
	}

	public static Rect CenteredRectHigh() {
		float width = Screen.width / 5f;
		float height = Screen.height / 5f;
		float left = (Screen.width / 2f) - (width / 2f);
		float top = (Screen.height / 2f) - ((height / 2f) *2);
		return new Rect(left, top, width, height);
	}

	public static Rect CenteredRect() {
		float width = Screen.width / 5f;
		float height = Screen.height / 5f;
		float left = (Screen.width / 2f) - (width / 2f);
		float top = (Screen.height / 2f) - (height / 2f);
		return new Rect(left, top, width, height);
	}

	// Can handle 8 options for now
	private static Rect[] optionRects = null;
	public static Rect OptionRect(int option) {
		if (optionRects == null) {
			initOptionsRects ();
		}
		return optionRects [option];
	}

	public const int MAX_OPTIONS = 8;
	private static void initOptionsRects() {
		float height = Screen.height / 10;
		float width = Screen.width / 3;
		float left = (Screen.width / 2) - (width / 2); 
		optionRects = new Rect[MAX_OPTIONS];
		for (int i = 0; i < MAX_OPTIONS; i++) {
			optionRects[i] = new Rect (left, height * (i + 1), width, height);
		}
	}


}
