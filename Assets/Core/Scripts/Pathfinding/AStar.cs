using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AStar {

	Node current, start, goal;
	List<Node> closed, open;
	Dictionary<Node, Node> visited;

	bool foundPath;

	public AStar () {
		closed = new List<Node> ();
		open = new List<Node> ();
		visited = new Dictionary<Node, Node> ();
	}

	public List<Vector2> GetShortestPath (Node from, Node to, List<Node> nodes) {
		this.start = from;
		this.goal = to;
		open.Add (from);

		while (open.Count > 0) {
			current = GetBestNodeFromOpenList ();
			if (current == goal) {
				Debug.Log ("Path already found");
			}

			open.Remove (current);
			closed.Add (current);

			foreach (Node neighbor in current.GetNeighbors()) {
				if (closed.Contains (neighbor)) {
					continue;
				}

				if (!open.Contains (neighbor)) {
					open.Add (neighbor);
					SetScoresFor (neighbor);
					visited.Add (neighbor, current);
				} else {
					float altF = DistanceBetween(current, neighbor);
					if (neighbor.G < neighbor.H + altF) {
						visited[neighbor] = current;
						SetScoresFor(neighbor, true);
					}
				}
			}
		}
		return ConstructPath ();
	}

	private Node Clone(Node node) {
		Node clone = null;
		try {
			clone = node.Clone();
		} catch (System.ArgumentException) {
			Debug.LogError ("Node needs to be serializable. Add attribute [Serializable] to " + node.GetType());
		}
		return clone;
	}

	private List<Vector2> ConstructPath () {
		List<Vector2> path = new List<Vector2> (); // { goal };
		path.Add (goal.GetPosition());
		Node current = goal;
		Node parent = null;

		bool pathFound = true;
		while (!path.Contains(start.GetPosition()) && pathFound) {
			parent = visited[current];
			if (parent != null) {
				path.Add (parent.GetPosition());
			} else {
				pathFound = false;
			}
			current = parent;
		}

		if (!pathFound) {
			Debug.LogWarning ("No path from " + start + " to " + goal + " was found");
		}
		path.Reverse();
		return path;
	}

	private void SetScoresFor(Node node) {
		SetScoresFor(node, false);
	}

	private void SetScoresFor(Node node, bool ignoreHueistic) {
		Node parent = null;
		try {
			parent = visited[node];
		} catch (System.Exception) {
			parent = node;
		}
		node.G = node.G + DistanceBetween (node, parent);
		if (!ignoreHueistic) {
			node.H = DistanceBetween (node, goal);
		}
		node.F = node.G + node.H;
	}

	private float DistanceBetween (Node source, Node target) {
		return PositionUtils.GetDistanceBetween2D (source.GetPosition (), target.GetPosition ());
	}

	private Node GetBestNodeFromOpenList () {
		Node best = open [0];
		foreach (Node node in open) {
			if (node.F < best.F) {
				best = node;
			}
		}
		return best;
	}


}
