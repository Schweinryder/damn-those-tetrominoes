using UnityEngine;
using System.Collections.Generic;
using System;

public class CustomNode : Node {

	public float F { get; set; }
	public float G { get; set; }
	public float H { get; set; }

	public Node parent { get; set; }

	Vector2 position;
	List<Node> neighbors;

	public CustomNode(Vector2 position, List<Node> neighbors) {
		this.position = position;
		this.neighbors = neighbors;
	}

	public Vector2 GetPosition() {
		return position;
	}

	public List<Node> GetNeighbors() {
		return neighbors;
	}

}
