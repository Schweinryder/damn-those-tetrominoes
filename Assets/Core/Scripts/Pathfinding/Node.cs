using UnityEngine;
using System.Collections.Generic;

public interface Node {

	float F { get; set; } 
	float G { get; set; }
	float H { get; set; }

	Vector2 GetPosition();

	List<Node> GetNeighbors();

}

