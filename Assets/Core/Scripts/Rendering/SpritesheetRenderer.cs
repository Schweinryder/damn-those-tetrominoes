﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(SpriteRenderer))]
public abstract class SpritesheetRenderer : MonoBehaviour {

	public enum Mode {
		LOOP,
		BACK_AND_FORTH,
		ONE_ITERATION
	}

	protected Mode mode;

	public float TimeBetweenSprites { get; set; }

	private SpriteRenderer spriteRenderer;
	private List<Sprite> sprites;
	private Sprite defaultSprite;
	private int nextSprite;

	private int indexFrom;
	private int indexTo;
	private bool isCountingUp;

	private bool isRendering;
	private float timer;

	void Awake () {
		spriteRenderer = GetComponent<SpriteRenderer> ();
		TimeBetweenSprites = .2f;
		mode = Mode.LOOP;
	}

	public void MoveToX (float newX) {
		transform.position = new Vector3 (newX, transform.position.y, transform.position.z);
	}

	public void SetDefaultSprite(int index) {
		SetDefaultSprite (sprites [index]);
	}

	public void SetDefaultSprite(Sprite sprite) {
		this.spriteRenderer.sprite = sprite;
	}

	public void SetSprite(int index) {
		SetSprite (sprites [index]);
	}

	public void SetSprite(Sprite sprite) {
		this.spriteRenderer.sprite = sprite;
		this.isRendering = false;
	}

	public void SetSprites(List<Sprite> sprites) {
		defaultSprite = spriteRenderer.sprite;
		this.sprites = sprites;
	}

	public void RemoveDefaultSprite() {
		spriteRenderer.sprite = null;
	}

	public void StartRender (int from, int to) {
		indexFrom = nextSprite = from;
		indexTo = to;
		isRendering = true;
		timer = TimeBetweenSprites;
		isCountingUp = true;
	}

	public void StopRender () {
		if (spriteRenderer != null)
			spriteRenderer.sprite = defaultSprite;
		isRendering = false;
	}

	// Update is called once per frame
	void Update () {
		if (isRendering) {
			if (timer <= 0) {
				UpdateSprite ();
				UpdateSpriteIndex ();
				timer = TimeBetweenSprites;
			} else {
				timer -= Time.deltaTime;
			}
		}	
	}

	void UpdateSprite() {
		spriteRenderer.sprite = GetNextSprite ();
	}

	Sprite GetNextSprite() {
		return sprites [nextSprite];
	}

	void UpdateSpriteIndex() {
		switch (mode) {
		case Mode.BACK_AND_FORTH:
			if (isCountingUp) {
				nextSprite++;
				if (nextSprite >= indexTo) {
					nextSprite = indexTo;
					isCountingUp = false;
				}
			} else {
				nextSprite--;
				if (nextSprite < indexFrom) {
					nextSprite = indexFrom;
					isCountingUp = true;
				}
			}
			break;
		case Mode.LOOP:
			nextSprite++;
			if (nextSprite >= indexTo) {
				nextSprite = indexFrom;
			}
			break;
		case Mode.ONE_ITERATION:
			nextSprite++;
			if (nextSprite > indexTo) {
				SetSprite (spriteRenderer.sprite);
			}
			break;
		}
	}
}
