﻿using UnityEngine;
using System.Collections;

public class GameObjectService : MonoBehaviour {

	public static GameObject SpawnObject(MonoBehaviour behaviour) {
		return SpawnObject (behaviour.gameObject);
	}

	public static GameObject SpawnObject(GameObject prefab) {
		GameObject spawnedObject = GameObject.Instantiate (prefab);
		InitializationUtils.InitializeObject (spawnedObject);
		return spawnedObject;
	}

	public static GameObject SpawnObject(GameObject prefab, float y, float x) {
		Vector3 position = prefab.transform.position;
		GameObject spawnedObject = GameObject.Instantiate (prefab, new Vector3(x, y, position.z), Quaternion.identity) as GameObject;
		InitializationUtils.InitializeObject (spawnedObject);
		return spawnedObject;
	}

	public static void RemoveObject (GameObject obj) {
		Destroy (obj);
	}

}
