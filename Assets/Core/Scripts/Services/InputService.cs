﻿using UnityEngine;
using System.Collections;

public class InputService : MonoBehaviour {

	const float HOLD_TIME = .2f;

	public enum MouseState {
		NO_INPUT,
		MOUSE_CLICK,
		MOUSE_HOLD
	}

	public static MouseState mouseState { get; private set; }

	bool mouseIsDown, mouseWasDown;
	float mousedownTimer;

	void Awake () {
		mousedownTimer = 0f;
	}

	void Update() {
		mouseIsDown = Input.GetMouseButton(0);
		if (mouseIsDown && mouseWasDown) {
			mousedownTimer += mousedownTimer + Time.deltaTime;
			if (mouseState == MouseState.MOUSE_CLICK) {
				mouseState = MouseState.NO_INPUT;
			}
			if (mousedownTimer > HOLD_TIME) {
				mouseState = MouseState.MOUSE_HOLD;
			}
		} else if (mouseIsDown) {
			mousedownTimer = 0;
			mouseState = MouseState.MOUSE_CLICK;
		} else {
			mouseState = MouseState.NO_INPUT;
		}
		mouseWasDown = mouseIsDown;
	}
	
	static bool IsHoldingMouse() {		
		return false;
	}

	public static Vector3 GetMousePosition() {
		return Camera.main.ScreenToWorldPoint(Input.mousePosition);
	}

}
