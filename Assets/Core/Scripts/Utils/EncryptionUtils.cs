﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public static class EncryptionUtils {

	private static string word = "peaceisalie";

	public static string Encrypt (string stringToEncrypt) {
		return simpleXOREncryptDecrypt (stringToEncrypt);
	}

	public static string Decrypt (string stringToDecrypt) {
		return simpleXOREncryptDecrypt (stringToDecrypt);
	}

	/// <summary>
	/// Simple XOR encryption/decryption method.
	/// </summary>
	/// <returns>Encrypted text if decrypted text is passed, decypted text if encrypted text is passed</returns>
	/// <param name="str">String.</param>
	private static string simpleXOREncryptDecrypt(string str) {
		StringBuilder inSb = new StringBuilder(str);
		StringBuilder outSb = new StringBuilder(str.Length);
		char c;
		for (int i = 0; i < str.Length; i++)
		{
			c = inSb[i];
			c = (char)(c ^ 129);
			outSb.Append(c);
		}
		return outSb.ToString();
	}

}