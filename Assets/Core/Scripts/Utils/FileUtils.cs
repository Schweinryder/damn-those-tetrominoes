﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.IO;
using System;

public class FileUtils {
	

	private static BinaryFormatter formatter = new BinaryFormatter();

	private static string Encrypt(string str) {
		return EncryptionUtils.Encrypt (str);
	}

	private static string Dectrypt(string str) {
		return EncryptionUtils.Decrypt (str);
	}

	public static string LoadFileContent(string filename, bool decrypt) {
		string fileContent = "";
		if(FileExists(filename)) {
			try {
				FileStream file = OpenFile(filename);
				fileContent = Dectrypt ((string)formatter.Deserialize (file));
				file.Close();
			} catch {
				fileContent = "";
			}
		} else {
			Debug.LogWarning("File '" + filename + "' does not exist");
		}
		//Debug.Log ("Loading '" + fileContent + "'");
		return fileContent;
	}

	public static void SaveToFile(string content, string filename, bool encrypt) {
		try {
			if (encrypt) {
				//Debug.Log ("Saving '" + content + "'");
				FileStream fileStream = CreateFile(filename);
				formatter.Serialize (fileStream, Encrypt (content));
				fileStream.Close ();
			} else {
				SaveToFile(content, filename);
			}
		} catch (Exception e) {
			Debug.LogWarning ("Warning: " + e.Message);
		}
	}

	public static void SaveToFile(string content, string filename) {
		File.WriteAllText(GetPath(filename), content);
	}

	private static FileStream CreateFile(string fileName) {
		return File.Create (GetPath(fileName));
	}

	private static FileStream OpenFile(string fileName) {
		return File.Open (GetPath(fileName), FileMode.Open);
	}

	private static bool FileExists(string fileName) {
		return File.Exists(GetPath(fileName));
	}

	private static string GetPath(string filename) {
		string path = Application.persistentDataPath + "/" + filename;
		//Debug.Log(path);
		return path;
		/*StringBuilder pathBuilder = new StringBuilder(Application.persistentDataPath);
		if (!pathBuilder.ToString().EndsWith(separator)) {
			pathBuilder.Append(separator);
		}
		string path = pathBuilder.ToString();
		Debug.Log ("Sparator: '" + separator + "'");
		Debug.Log (path);
		return path;*/
	}

}
