﻿using UnityEngine;
using System.Collections;

public class InitializationUtils {

	public static void InitializeObject(GameObject gameObject) {
		RemoveCloneName(gameObject);
	}

	private static void RemoveCloneName(GameObject gameObject) {
		int cloneIndex = gameObject.name.IndexOf("(");
		if (cloneIndex > 0) {
			gameObject.name = gameObject.name.Substring(0, cloneIndex); // Remove '(Clone)' from name
		}
	}

}
