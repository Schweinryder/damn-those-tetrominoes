﻿using UnityEngine;
using System.Collections;
using System;

public static class MathUtils {

	public static float Round(float value) {
		return (Mathf.Round(value * 10) / 10);
	}

	public static Vector2 Round(Vector2 vector2) {
		return new Vector2(Round (vector2.x), Round (vector2.y));
	}

	public static float BigRound(float value) {
		return (Mathf.Round(value * 1000) / 1000);
	}

}