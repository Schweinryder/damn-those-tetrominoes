﻿using UnityEngine;
using System.Collections;
using System;

public static class PositionUtils {

	public static float GetDistanceBetween2D (Vector3 position, Vector3 otherPosition) {
		return (float) Math.Sqrt((Math.Pow(position.x - otherPosition.x, 2) + Math.Pow(position.y - otherPosition.y, 2)));
	}
	
	public static float GetAngleFrom(Vector3 position, Vector3 root) {
		return Mathf.Atan2(position.y - root.y, position.x - root.x) * Mathf.Rad2Deg;
	}

}
