﻿using UnityEngine;
using System.Collections;

public static class ScreenUtils {

	public static float HeightPercent(float percent) {
		return (float)Screen.height * (percent / 100);
	}

	public static float WidthPercent(float percent) {
		return (float)Screen.width * (percent / 100);
	}

	public static float MidPointX () {
		return Screen.width / 2f;
	}

	public static float MidPointY () {
		return Screen.height / 2f;
	}

}
