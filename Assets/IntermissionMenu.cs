﻿using UnityEngine;
using System.Collections;

public class IntermissionMenu : MonoBehaviour {

	ValueText valueText;

	void Awake () {
		valueText = GetComponentInChildren<ValueText> ();
	}

	public void SetValue (int value) {
		valueText.SetValue (value);
	}


	static readonly Vector3 ShowPosition = new Vector3 (2, .5f, -9);
	static readonly Vector3 HidePosition = new Vector3 (10000, 10000, -9);

	public void Hide () {
		transform.position = HidePosition;
	}

	public void Show () {
		transform.position = ShowPosition;
	}
}
