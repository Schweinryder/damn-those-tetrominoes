﻿using UnityEngine;
using System.Collections;

public class LevelPrinter : MonoBehaviour {

	ValueText valueText;

	void Awake () {
		valueText = GetComponentInChildren<ValueText> ();
	}

	public void SetValue (string value) {
		valueText.SetValue (value);
	}

	void Update () {
		SetValue (GameController.level.ToString ());
	}

}
