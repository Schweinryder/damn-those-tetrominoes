﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour {

	Transform player;
	bool zoomOut;
	
	// Update is called once per frame
	void Update () {
		if (GameController.gameState == GameState.MARIO) {
			verifyPlayer ();
			if (Input.GetKey (KeyCode.E)) {
				zoomOut = true;
				transform.position = new Vector3 (8.5f, 8.4f, -100);
				GetComponent<Camera> ().orthographicSize = 10;
			} else {
				zoomOut = false;
				GetComponent<Camera> ().orthographicSize = 4;
			}
		}



	}

	public void OnGameStateChange () {
		zoomOut = false;
		switch (GameController.gameState) {
		case GameState.TETRIS:
		case GameState.MENUS:
			transform.position = new Vector3 (8.5f, 8.4f, -100);
			GetComponent<Camera> ().orthographicSize = 10;
			break;
		case GameState.MARIO:
			GetComponent<Camera> ().orthographicSize = 4;
			break;
		}
	}

	void verifyPlayer () {
		if (player == null) {
			player = FindObjectOfType<Player> ().transform;
		}
	}

	void LateUpdate () {
		if (player != null && GameController.gameState == GameState.MARIO && !zoomOut)
		transform.position = new Vector3 (player.position.x, player.position.y, transform.position.z);
	}

}
