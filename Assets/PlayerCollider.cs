﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class PlayerCollider : MonoBehaviour {

	const float Step = 1f;

	const string Left = "left";
	const string Right = "right";
	const string Top = "top";
	const string Bottom = "bottom";
	const string Floor = "floor";
	const string Finish = "Finish";

	Player player;
	Rigidbody2D body;
	List<string> currentCollisions;

	void Awake() {
		player = GetComponent<Player> ();
		body = GetComponent<Rigidbody2D> ();
		Debug.Assert (player != null, "Player is null");
		Debug.Assert (body != null, "Bpdy is null");
		currentCollisions = new List<string> ();
	}

	void OnCollisionEnter2D (Collision2D other) {
		//Log ("Player collided with " + other.gameObject.name);
		body.velocity = new Vector2(0, body.velocity.y);
		currentCollisions.Add (other.gameObject.name);
	}

	void OnCollisionExit2D (Collision2D other) {
		//Log ("No longer contact with " + other.gameObject.name);
		currentCollisions.Remove (other.gameObject.name);
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.tag.Equals(Finish)) {
			FindObjectOfType<GameController> ().PlayerWin ();
			return;
		} 
		Log ("Triggered");
		/*if (transform.position.y > other.transform.position.y) {
			return;
		}*/
		Block block = other.GetComponent<Block> ();
		if (block != null) {
			CollideWith(block);
		}
	}

	void CollideWith(Block block) {
		Direction direction = block.direction;
		Vector3 validPosition;

		switch (direction) {
		case Direction.Undefined:
			validPosition = HandleCollisionWithRotating(block, direction);
			break;
		case Direction.NoDirection:
			validPosition = HandleCollisionWithFixed(block, direction);
			break;
		default:
			validPosition = HandleCollisionWithFalling(block, direction);
			break;
		}

		Log ("Direction: " + direction + ", Old: " + transform.position + ", New: " + validPosition);
		currentCollisions.Clear();
		transform.position = validPosition;
		body.isKinematic = true;
		player.canMove = false;
		Log ("Player is now kinematic");
	}

	Vector3 HandleCollisionWithFalling(Block block, Direction direction) {	
		float xStep = transform.position.x;
		float yStep = transform.position.y;	

		switch (direction) {
		case Direction.Up:
			Log ("This is impossible, block upwards movement isn't even supported");
			break;
		case Direction.Down:
			if (block.transform.position.y > 0) {
				yStep = block.transform.position.y - Step;
				currentCollisions.Remove (Top);
			}
			break;
		case Direction.Left:
			xStep = block.transform.position.x - Step;
			currentCollisions.Remove (Right);
			Log ("Move player left");
			break;
		case Direction.Right:
			xStep = block.transform.position.x + Step;
			currentCollisions.Remove (Left);
			Log ("Move player right");
			break;
		}

		return new Vector3 (xStep, yStep, transform.position.z);
	}

	Vector3 HandleCollisionWithRotating(Block block, Direction direction) {
		Log ("Hit by rotation");
		return GetBestValidPosition(block, direction);
	}

	Vector3 HandleCollisionWithFixed(Block block, Direction direction) {
		Log ("Entered fixed trigger");
		return GetBestValidPosition(block, direction);
	}

	Vector3 GetBestValidPosition(Block block, Direction direction) {
		float xStep = transform.position.x;
		float yStep = transform.position.y;	
		
		float yDiff = MathUtils.Round(transform.position.y - block.transform.position.y);
		float xDiff = MathUtils.Round(transform.position.x - block.transform.position.x);
		Log ("DIFFS: " + xDiff + ", " + yDiff);
		if (yDiff < 0 && yDiff > -1f) {
			yStep = block.transform.position.y - Step;
		} else if (yDiff > 0 && yDiff < 1f) {
			yStep = block.transform.position.y + Step;
		}
		if (xDiff < 0 && xDiff > -1f) {
			xStep = block.transform.position.x - Step;
			
		} else if (xDiff > 0 && xDiff < 1f) {
			xStep = block.transform.position.x + Step;
			
		}
		return new Vector3 (xStep, yStep, transform.position.z);
	}

	void PrintCollisions () {
		StringBuilder sb = new StringBuilder ("Collisions: ");
		foreach (string col in currentCollisions) {
			sb.Append (col).Append (", ");
		}
		sb.Append (".");
		Log (sb.ToString ());
	}

	void Update () {
		if ((currentCollisions.Contains(Top) && currentCollisions.Contains (Bottom)) ||
			(currentCollisions.Contains(Top) && currentCollisions.Contains (Floor))) {
			Log ("Player is kill");
			PrintCollisions ();
			Debug.Break();
			FindObjectOfType<GameController> ().KillPlayer ();
		}
		if (currentCollisions.Contains(Left) && currentCollisions.Contains (Right)) {
			Log ("Player is side kill");
			Debug.Break();
			FindObjectOfType<GameController> ().KillPlayerSide ();
		}
	}

	static int log = 0;
	public void Log(string phrase) {
		Debug.Log (++log + ": " + phrase);
	}

}
