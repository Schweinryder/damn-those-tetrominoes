﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	public GameObject playerPrefab;
	public static GameState gameState;

	Player player;
	MainCamera mainCamera;

	public static int level;

	public const float TetrominoTime = 120;
	public const float MarioExtraTime = 10; // = 10;

	public static float RoundTime;
	public static float TimeSinceRoundStart;

	StartMenu startMenu;
	IntermissionMenu interMenu;
	GameOverMenu gameOverMenu;

	TetrominoFactory factory;

	void Start () {
		mainCamera = FindObjectOfType<MainCamera> ();
		startMenu = FindObjectOfType<StartMenu> ();
		interMenu = FindObjectOfType<IntermissionMenu> ();
		gameOverMenu = FindObjectOfType<GameOverMenu> ();
		factory = FindObjectOfType<TetrominoFactory> ();
		level = 1;
		SetGameState (GameState.MARIO);
	}
	
	public void SetGameState (GameState newState) {
		gameState = newState;
		mainCamera.OnGameStateChange ();
		TimeSinceRoundStart = 0;
		if (gameState == GameState.MARIO) {
			SpawnPlayer ();
			Grid.SetMarioMode ();
			RoundTime = TetrominoTime + MarioExtraTime;
		}
		if (gameState == GameState.TETRIS) {
			factory.PrepareNextLevel ();
			RoundTime = TetrominoTime;
		}
	}

	void SpawnPlayer() {
		player = Instantiate (playerPrefab).GetComponent<Player> ();
	}
	
	// Update is called once per frame
	void Update () {
		switch (gameState) {
		case GameState.MENUS:
			UpdateMenus ();
			break;
		case GameState.TETRIS:
			UpdateTetris ();
			break;
		case GameState.MARIO:
			UpdateMario ();
			break;
		}
	}

	void UpdateMenus () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			startMenu.Hide ();
			interMenu.Hide ();
			gameOverMenu.Hide ();
			SetGameState (GameState.TETRIS);
		}
	}

	void UpdateTetris () {
		TimeSinceRoundStart += Time.deltaTime;
		if (TimeSinceRoundStart >= RoundTime) {
			SetGameState (GameState.MARIO);
		}
		if (Input.GetKeyDown (KeyCode.R)) {
			SetGameState (GameState.MARIO);
		}
	}

	void UpdateMario () {
		TimeSinceRoundStart += Time.deltaTime;
		if (TimeSinceRoundStart >= RoundTime) {
			KillPlayer (false);
		}
		if (Input.GetKeyDown (KeyCode.R)) {
			KillPlayer (false);
		}
	}

	public void PlayerWin () {
		interMenu.Show ();
		interMenu.SetValue (level);
		Grid.EndRound ();
		factory.PrepareNextLevel ();
		Destroy (player.gameObject);
		SetGameState (GameState.MENUS);
		level++;
	}

	public void KillPlayer () {
		//KillPlayer (true);
		player.CrushDown ();
	}

	public void KillPlayerSide () {
		player.CrushSide ();
	}

	private void KillPlayer (bool gotCrushed) {
		gameOverMenu.Show ();
		if (gotCrushed) {
			gameOverMenu.SetValue ("You got crushed");
		} else {
			gameOverMenu.SetValue ("Timer ran out");
		}
		Grid.EndRound ();
		factory.PrepareNextLevel ();
		Destroy (player.gameObject);
		SetGameState (GameState.MENUS);
		level = 1;
	}

}
