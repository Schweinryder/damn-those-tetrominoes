﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockService : MonoBehaviour {

	static readonly Vector3 HiddenPosition = new Vector3 (-100, -100, 10);

	public Transform blockPrefab;

	private List<Transform> blockList;
	private int nextIndex;

	void Start () {
		PrepareBlocks ();
	}

	public void PrepareBlocks() {
		blockList = new List<Transform> ();
		int blockCount = Grid.Height * Grid.Width;
		nextIndex = 0;
		for (int i = 0; i < blockCount; i++) {
			Transform blockObject = Instantiate (blockPrefab.gameObject).transform;
			blockObject.transform.position = HiddenPosition;
			blockList.Add (blockObject);
		}
	}

	public void PlaceNewBlock (GridHistory.CellData cell) {
		Block block = blockList[nextIndex].GetComponent<Block> ();
		block.SetPosition (cell.position);
		block.SetDirection (cell.direction);
		block.isFalling = cell.isFalling;
		nextIndex++;
	}

	public void PlaceBlocks (GridHistory.GridData gridData) {
		
		HideAll ();
		foreach (GridHistory.CellData cell in gridData.populatedCells) {
			PlaceNewBlock (cell);
		}
	}

	public void HideAll () {
		foreach (Transform t in blockList) {
			t.position = HiddenPosition;
		}
		nextIndex = 0;
	}


}
