﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System;

public class Grid : MonoBehaviour {

	public enum State {
		UNDEFINED,
		WORKING,
		WAITING,
		GAME_OVER
	}

	public enum Mode {
		UNDEFINED,
		RECORD,
		PLAYBACK,
		WAITING_FOR_PLAYER
	}

	private const float TILE_DISTANCE = 1f;

	public const int Width = 10;
	public const int Height = 18;

	private static Transform[,] grid = new Transform[Width, Height];

	public static State state;
	public static Mode mode;

	private static BlockService blockService;
	private static GridHistory gridHistory;
	private static GridHistory.GridData currentGrid, nextGrid;
	private static float timeToNextGrid;
	private static float timeSinceStart;

	void Awake () {
		state = State.WAITING;
		mode = Mode.RECORD;
		gridHistory = new GridHistory ();
		timeSinceStart = 0;
	}

	void Start () {
		blockService = FindObjectOfType<BlockService> ();
	}

	void Update () {
		switch (mode) {
		case Mode.RECORD:
			UpdateRecord ();
			break;
		case Mode.PLAYBACK:
			UpdatePlayback ();
			break;
		case Mode.WAITING_FOR_PLAYER:
			UpdateWaitingForPlayer ();
			break;
		}
	}

	void UpdateRecord () {
		switch (state) {
		case State.WAITING:
			timeSinceStart += Time.deltaTime;
			break;
		case State.WORKING:
			UpdateWork ();
			break;
		}
	}

	void UpdatePlayback () {
		switch (state) {
		case State.WAITING:
			timeToNextGrid -= Time.deltaTime;
			if (timeToNextGrid <= 0) {
				GridHistory.GridData gridData = gridHistory.getNextGrid ();
				if (gridData != null) {
					timeToNextGrid = gridData.time - GameController.TimeSinceRoundStart;
					blockService.PlaceBlocks (gridData);
					DeleteFullRows (false);
				} else {
					Debug.Log ("No more data. Wait for player");
					mode = Mode.WAITING_FOR_PLAYER;
				}
			}
			break;
		case State.WORKING:
			UpdateWork ();
			break;
		}		
	}

	void UpdateWork () {
		bool done = CheckIfDone ();
		if (done) {
			DeleteFullRows (true);
		}
	}

	void UpdateWaitingForPlayer () {
		
	}

	bool CheckIfDone () {
		for (int y = 0; y < Grid.Height; ++y) {
			for (int x = 0; x < Grid.Width; ++x) {
				if (IsBlinking (x, y)) {
					return false;
				}
			}
		}
		return true;
	}

	bool IsBlinking (int x, int y) {
		if (grid[x, y] != null) {
			return grid [x, y].GetComponent<Block> ().state == Block.State.BLINKING;
		}
		return false;
	}

	public static bool IsInsideBorder(Vector2 pos) {
		return (pos.x >= 0 &&
			pos.x < 10 &&
			pos.y >= 0);
	}

	private static void DeleteRow(int y) {
		for (int x = 0; x < Width; ++x) {
			Destroy(grid[x, y].gameObject);
			grid[x, y] = null;
		}
	}

	public static void DecreaseRow(int y) {
		for (int x = 0; x < Width; ++x) {
			if (grid[x, y] != null) {
				// Move one towards bottom
				grid[x, y-1] = grid[x, y];
				grid[x, y] = null;

				// Update Block position
				grid[x, y-1].position += new Vector3(0, -1f, 0);
			}
		}
	}

	public static void DecreaseRowsAbove(int y) {
		for (int i = y; i < Height; ++i)
			DecreaseRow(i);
	}

	public static bool IsRowFull(int y) {
		for (int x = 0; x < Width; ++x)
			if (grid[x, y] == null)
				return false;
		return true;
	}

	public static void DeleteFullRows() {
		DeleteFullRows (false);
	}

	private static void DeleteFullRows (bool deleteNow) {
		for (int y = 0; y < Height; ++y) {
			if (IsRowFull(y)) {
				if (deleteNow) {
					DeleteRow (y);
					DecreaseRowsAbove (y + 1);
					--y;
					state = State.WAITING;
				} else {
					RenderDeleteAnimation (y);
					state = State.WORKING;
				}
			}
		}
	}

	public static void SetMarioMode () {
		mode = Mode.PLAYBACK;
		gridHistory.prepareForPlayback ();
		ClearGrid ();
	}

	public static void EndRound () {
		ClearGrid ();
		gridHistory.clear ();
		blockService.HideAll ();
		mode = Mode.RECORD;
	}

	public static void ClearGrid () {
		for (int i = 0; i < Grid.Width; i++) {
			for (int j = 0; j < Grid.Height; j++) {
				Transform t = grid [i, j];
				if (t != null) {
					Destroy (t.gameObject);
					t = null;
				}
			}
		}
	}

	private static void RenderDeleteAnimation (int y) {
		for (int x = 0; x < Width; ++x) {
			Block block = grid[x, y].GetComponent<Block> ();
			block.RenderRemoveAlteration ();
		}
	}

	public static void UpdateGrid(Transform transform, Direction direction) {
		// Remove old children from grid
		for (int y = 0; y < Grid.Height; ++y)
			for (int x = 0; x < Grid.Width; ++x)
				if (Grid.grid[x, y] != null)
				if (Grid.grid[x, y].parent == transform)
					Grid.grid[x, y] = null;

		// Add new children to grid
		foreach (Transform child in transform) {
			Vector2 v = MathUtils.Round(child.position);
			int gridX = (int)(Mathf.Round(v.x));
			int gridY = (int)(Mathf.Round(v.y));
			Grid.grid [gridX, gridY] = child;
		}

		PrintGridToFile (grid);
		gridHistory.saveCurrentGrid (grid, GameController.TimeSinceRoundStart, direction);
	}

	const string Path = @"C:\Users\Webe\Documents\Projects\Unity\grid.txt";
	public static void PrintGridToFile(Transform[,] grid) {
		StringBuilder sb = new StringBuilder ();
		for (int i = Height - 1; i >= 0; i--) {
			sb.Append (GetRowAsString (grid, i));
		}

		File.WriteAllText (Path, sb.ToString ());
	}

	public static string GetRowAsString(Transform[,] grid, int row) {
		StringBuilder sb = new StringBuilder ();
		for (int j = 0; j < Width; ++j) {
			if (grid[j, row] == null) {
				sb.Append ("O");
			} else {
				sb.Append ("X");
			}
		}
		sb.Append ("\n");
		return sb.ToString ();
	}

	public static bool IsPositionFree (Transform transform, Vector2 position) {
		int gridX = ToGridInt (position.x);
		int gridY = ToGridInt (position.y);
		if (grid [gridX, gridY] != null && grid [gridX, gridY].parent != transform) {
			return false;
		}
		return true;
	}

	public static int ToGridInt (float value) {
		return (int)(Mathf.Round(value));
	}
}











































