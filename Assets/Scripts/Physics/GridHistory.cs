﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GridHistory {

	List<GridData> previousGrids = new List<GridData> ();
	int index;

	public void clear () {
		previousGrids.Clear ();
		index = 0;
	}
		
	public void saveCurrentGrid(Transform[,] grid, float time, Direction direction) {
		previousGrids.Add (new GridData (grid, time, direction));
	}

	public void prepareForPlayback () {
		index = 0;
		//Debug.Log ("Nr of entries: " + previousGrids.Count);
	}

	public int count () {
		return previousGrids.Count;
	}

	public GridData getNextGrid() {
		if (index >= previousGrids.Count) {
			return null;
		}
		GridData gridData = previousGrids [index++];
		return gridData;
	}

	public class GridData {

		public List<CellData> populatedCells;
		public float time { get; private set; }
		
		public GridData (Transform[,] tGrid, float time, Direction direction) {
			//Debug.Log ("Entry direction: " + direction);
			this.time = time;
			saveGrid(tGrid, direction);
		}

		private void saveGrid(Transform[,] tGrid, Direction direction) {
			this.populatedCells = new List<CellData> ();
			for (int i = 0; i < Grid.Width; i++) {
				for (int j = 0; j < Grid.Height; j++) {
					Transform t = tGrid [i, j];
					if (t != null) {
						bool isFalling = t.GetComponentInParent<Tetromino> ().IsFalling ();
						Direction cellDir;
						if (isFalling) {
							cellDir = direction;
						} else {
							cellDir = Direction.NoDirection;
						}
						CellData cell = new CellData (MathUtils.Round (t.position), cellDir, isFalling);
						populatedCells.Add (cell);
					}
				}
			}
		}
	}

	public class CellData {

		public Vector2 position { get; private set; }
		public Direction direction { get; private set; }
		public bool isFalling { get; private set; }

		public CellData(Vector2 position, Direction direction, bool isFalling) {
			this.position = position;
			this.direction = direction;
			this.isFalling = isFalling;
		}

	}

}
