﻿using UnityEngine;
using System.Collections;

public class MouseInput : MonoBehaviour {

	public static bool ClickedJump () {
		if (Input.GetMouseButtonDown(0) && Input.mousePosition.y > ScreenUtils.MidPointY ()) {
			return true;
		}
		return false;
	}

	public static Side ClickedSide () {
		Vector3 mousePos = Input.mousePosition;
		if (mousePos.y > ScreenUtils.MidPointY()) {
			return Side.Undefined;
		}
		if (mousePos.x > ScreenUtils.MidPointX()) {
			return Side.Right;
		} else {
			return Side.Left;
		}
	}

	public static bool ClickedRight () {
		return ClickedSide() == Side.Right && Input.GetMouseButton(0);
	}

	public static bool ClickedLeft () {
		return ClickedSide() == Side.Left && Input.GetMouseButton(0);
	}

}
