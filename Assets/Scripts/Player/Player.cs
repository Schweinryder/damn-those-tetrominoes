﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {
	
	const float MaxCollisionHeight = 17f;

	public const float Width = .5f;
	public const float Height = 1f;

	enum State {
		IDLE,
		RUNNING,
		JUMPING,
		FALLING,
		CRUSHED_DOWN,
		CRUSHED_SIDE
	}

	const float MaxX = 9f;
	const float minX = 0;

	public Vector3 position { 
		get { return transform.position; }
		set { transform.position = value;}
	}

	public float JumpPower = 800;
	const float Speed = 10;
	public bool canMove { get; set; }

	public static Vector2 velocity;

	State state;
	PlayerSpritesheetRenderer spriteRenderer;
	Rigidbody2D body;
	PlayerCollider playerCollider;

	void Awake () {
		spriteRenderer = GetComponentInChildren<PlayerSpritesheetRenderer> ();
		playerCollider = GetComponent<PlayerCollider> ();
		body = GetComponent<Rigidbody2D> ();
		velocity = new Vector2(0, 0);
	}

	void SetDirection() {
		Vector3 scale = transform.localScale;
		float scaleX = scale.x;
		if (scale.x > 0 && body.velocity.x < 0) {
			scaleX = scale.x * -1;
		} else if (scale.x < 0 && body.velocity.x > 0) {
			scaleX = scale.x * -1;
		}
		transform.localScale = new Vector3 (scaleX, scale.y, scale.z);
	}

	void Update () {
		if (state == State.CRUSHED_DOWN) {
			return;
		}
		if (body.isKinematic) {
			body.isKinematic = false;
			playerCollider.Log ("Player is no more kinematci");
		}
		CheckInput ();
		SetDirection ();
	}

	void CheckInput () {
		switch (state) {
		case State.IDLE:
			OnIdle ();
			break;
		case State.RUNNING:
			OnRunning ();
			break;
		case State.JUMPING:
			OnJumping ();
			break;
		case State.FALLING:
			OnFalling ();
			break;
		}
	}

	void OnIdle () {
		bool isMoving = CheckMovement();
		if (isMoving) {
			ChangeStateTo (State.RUNNING);
		}
		CheckJumping();
	}

	void OnRunning () {
		bool isMoving = CheckMovement ();
		if (!isMoving) {
			ChangeStateTo (State.IDLE);
		}
		CheckJumping ();
	}

	void OnJumping () {
		CheckMovement ();
		if (body.velocity.y <= 0) {
			ChangeStateTo (State.FALLING);
		}
	}

	void OnFalling () {
		CheckMovement ();
		if (body.velocity.y == 0) {
			ChangeStateTo (State.IDLE);
		}
	}

	bool CheckMovement () {
		if ((Input.GetKey (KeyCode.A) || MouseInput.ClickedLeft()) && canMove) {
			Move (Speed * -1);
			return true;
		} else if ((Input.GetKey (KeyCode.D) || MouseInput.ClickedRight()) && canMove) {
			Move (Speed);
			return true;
		} else {
			Move (0);
			canMove = true;
			return false;
		}
	}


	void CheckJumping () {
		if (Input.GetKeyDown (KeyCode.W) || MouseInput.ClickedJump()) {
			Jump ();
		}
	}

	void ChangeStateTo (State newState) {
		state = newState;
		UpdateSpriteRenderer ();
	}

	void Move (float speed) {
		body.velocity = new Vector2 (speed, body.velocity.y);
	}

	void SetHeight (float y) {
		position = new Vector3(position.x, y, position.z);
	}

	void Jump () {
		body.AddForce (new Vector2 (0, JumpPower));
		ChangeStateTo (State.JUMPING);
	}

	void UpdateSpriteRenderer () {
		switch (state) {
		case State.IDLE:
			spriteRenderer.RenderIdle ();
			break;
		case State.RUNNING:
			spriteRenderer.RenderRun ();
			break;
		case State.JUMPING:
			spriteRenderer.RenderJump ();
			break;
		case State.FALLING:
			spriteRenderer.RenderFalling ();
			break;
		case State.CRUSHED_DOWN:
			spriteRenderer.RenderCrushed ();
			break;
		case State.CRUSHED_SIDE:
			spriteRenderer.RenderSideCrushed ();
			break;
		}
	}

	public void CrushDown () {
		ChangeStateTo (State.CRUSHED_DOWN);
	}

	public void CrushSide () {
		ChangeStateTo (State.CRUSHED_SIDE);
	}
	
	void LateUpdate () {
		transform.position = GetValidPosition (position);
	}

	Vector3 GetValidPosition (Vector3 position) {
		float xPos = position.x;
		float yPos = position.y;
		if ((xPos > MaxX + (Width / 2)) && (position.y < MaxCollisionHeight)) {
			xPos = MaxX + (Width / 2);
		}
		if ((xPos < minX - (Width / 2)) && (position.y < MaxCollisionHeight)) {
			xPos = minX- (Width / 2);
		}
		if (yPos < 0) {
			yPos = 0;
		}
		return new Vector3 (xPos, yPos, position.z);
	}

}
