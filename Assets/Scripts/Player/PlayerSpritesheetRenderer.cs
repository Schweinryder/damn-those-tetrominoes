﻿using UnityEngine;
using System.Collections;

public class PlayerSpritesheetRenderer : SpritesheetRenderer {

	public Sprite[] playerSprites;

	const int Idle = 0;
	const int RunStart = 1;
	const int RunEnd = 3;
	const int JumpStart = 4;
	const int JumpEnd = 5;
	const int Fall = 6;
	const int Crushed = 7;
	const int SideCrushed = 8;

	void Start () {
		SetSprites (ListUtils<Sprite>.ToList(playerSprites));
		RenderIdle ();
	}

	public void RenderIdle () {
		SetSprite (playerSprites [0]);
	}

	public void RenderRun () {
		this.mode = Mode.LOOP;
		StartRender (RunStart, RunEnd);
	}

	public void RenderJump () {
		TimeBetweenSprites = .1f;
		this.mode = Mode.ONE_ITERATION;
		StartRender (JumpStart, JumpEnd);
	}

	public void RenderFalling () {
		SetSprite (Fall);
	}

	public void RenderCrushed () {
		SetSprite (Crushed);
	}

	public void RenderSideCrushed () {
		SetSprite (SideCrushed);
	}
	
}