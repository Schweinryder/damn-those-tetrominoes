﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

	public const float Width = 1f;
	public const float Height = 1f;

	public Direction direction { get; private set; }
	public Vector3 previousPosition;
	public float x, y;
	public bool justMoved { get; private set; }

	public enum State {
		NORMAL,
		BLINKING
	}

	SpriteRenderer spriteRenderer;
	public Sprite[] sprites = new Sprite[2];

	public State state { get; private set; }

	public bool isFalling { get; set; }
	const float TimeBetweenSwitches = .05f;

	float switchTimer, notMovingTimer;
	int index, switches;

	void Awake () {
		direction = Direction.NoDirection;
		state = State.NORMAL;
		spriteRenderer = GetComponent<SpriteRenderer> ();
	}

	// Use this for initialization
	void Start () {
		index = 0;
	}

	public void SetPosition (Vector3 position) {
		previousPosition = transform.position;
		transform.position = position;
		justMoved = true;
	}

	public void SetDirection (Direction direction) {
		this.direction = direction;
		notMovingTimer = .2f;
	}

	public void RenderRemoveAlteration () {
		state = State.BLINKING;
		switches = 0;
	}
	
	// Update is called once per frame
	void Update () {
		justMoved = false;

		if (state == State.BLINKING) {
			switchTimer -= Time.deltaTime;
			if (switchTimer <= 0) {
				SwitchColor ();
				switchTimer = TimeBetweenSwitches;
			}
		}

		if (notMovingTimer > 0) {
			notMovingTimer -= Time.deltaTime;
			if (notMovingTimer <= 0) {
				direction = Direction.NoDirection;
				isFalling = false;
			}
		}
	}

	void SwitchColor () {
		if (index == 0) {
			index = 1;
		} else {
			index = 0;
		}
		spriteRenderer.sprite = sprites [index];
		switches++;
		if (switches >= 6) {
			state = State.NORMAL;
		}
	}

}
