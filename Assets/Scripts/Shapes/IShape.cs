﻿using UnityEngine;
using System.Collections;

public class IShape : Tetromino {

	bool rotated;

	public override void Rotate () {
		if (rotated) {
			RotateCounterClockwise ();
			rotated = false;
		} else {
			RotateClockwise ();
			rotated = true;
		}
	}

}
