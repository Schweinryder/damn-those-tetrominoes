﻿using UnityEngine;

using System.Collections;

public abstract class Tetromino : MonoBehaviour {

	enum State {
		INACTIVE,
		FALLING,
		FIXED_FALLING,
		FIXED
	}

	const float ROTATION = -90;
	const float FIXED_FALL_TIME = 0.1f;

	static readonly Vector3 SIDE_STEP = new Vector3 (1f, 0, 0);
	static readonly Vector3 DOWN_STEP = new Vector3 (0, 1f, 0);

	static readonly Vector3 DEFAULT_POSITION = new Vector3 (4f, 17f, 0);

	public TetrominoFactory factory { get; set; }
	State state = State.INACTIVE;

	const float BASE_TIME_BETWEEN_FALLS = 1f;
	public float TimeBetweenFalls { get; set; }
	float fallTimer;

	Vector3 oldPosition;
	Quaternion oldRotation;

	// Use this for initialization
	void Start () {
		
	}

	public void Activate (int level) {
		transform.position = DEFAULT_POSITION;
		state = State.FALLING;

		if (level == 1) {
			TimeBetweenFalls = BASE_TIME_BETWEEN_FALLS;
		} else if (level == 2) {
			TimeBetweenFalls = BASE_TIME_BETWEEN_FALLS - .25f;
		} else if (level == 3) {
			TimeBetweenFalls = BASE_TIME_BETWEEN_FALLS - .5f;
		} else {
			TimeBetweenFalls = BASE_TIME_BETWEEN_FALLS / level;
		}

		if (level < 20 && TimeBetweenFalls < .2f) {
			TimeBetweenFalls = .2f;
		} else if (level > 20 && TimeBetweenFalls < .1f) {
			TimeBetweenFalls = .1f;
		}
	}

	public bool IsFalling() {
		return state == State.FALLING || state == State.FIXED_FALLING;
	}
	
	// Update is called once per frame
	void Update () {
		if (GameController.gameState == GameState.MARIO) {
			Destroy (this.gameObject);
			return;
		}
		switch (state) {
		case State.FALLING:
			oldPosition = transform.position;
			oldRotation = transform.rotation;

			CheckPlayerInput ();

			fallTimer -= Time.deltaTime;
			if (fallTimer < 0) {
				FallOneStep ();
				fallTimer = TimeBetweenFalls;

				CheckGrid (true, Direction.Down);
			}
			break;
		case State.FIXED_FALLING:
			oldPosition = transform.position;

			if (Input.GetKeyDown(KeyCode.S)) {
				state = State.FALLING;
			}

			fallTimer -= Time.deltaTime;
			if (fallTimer < 0) {
				FallOneStep ();
				fallTimer = FIXED_FALL_TIME;

				CheckGrid (true, Direction.Down);
			}
			break;
		}

	}

	void CheckPlayerInput() {
		if (Input.GetKeyDown (KeyCode.A)) {
			MoveLeft ();
			CheckGrid (false, Direction.Left);
		} else if (Input.GetKeyDown (KeyCode.D)) {
			MoveRight ();
			CheckGrid (false, Direction.Right);
		} else if (Input.GetKeyDown (KeyCode.S)) {
			state = State.FIXED_FALLING;
			fallTimer = FIXED_FALL_TIME;
		} else if (Input.GetKeyDown (KeyCode.Space)) {
			Rotate ();
			CheckGrid (false, Direction.Undefined);
		}


	}

	void CheckGrid (bool setFixedOnInvalidPosition, Direction direction) {
		if (IsValidGridPos()) {
			Grid.UpdateGrid (this.transform, direction);
		} else {
			transform.position = oldPosition;
			transform.rotation = oldRotation;
			if (setFixedOnInvalidPosition) {
				state = State.FIXED;
				factory.OnShapeDone ();
			}
		}
	}

	bool IsValidGridPos() {        
		foreach (Transform child in transform) {
			Vector2 roundedPosition = MathUtils.Round(child.position);

			if (!Grid.IsInsideBorder (roundedPosition)) {
				return false;
			}

			if (!Grid.IsPositionFree (transform, roundedPosition)) {
				return false;
			}
		}
		return true;
	}

	bool canMoveDown() {
		return transform.position.y > 0;
	}

	void MoveLeft() {
		transform.position -= SIDE_STEP;
	}

	void MoveRight() {
		transform.position += SIDE_STEP;
	}

	void FallOneStep() {
		transform.position -= DOWN_STEP;
	}

	public virtual void Rotate() {
		RotateClockwise ();
	}

	protected void RotateClockwise () {
		transform.Rotate (new Vector3 (0, 0, ROTATION));
	}

	protected void RotateCounterClockwise () {
		transform.Rotate (new Vector3 (0, 0, ROTATION * -1));
	}

}
