﻿using UnityEngine;
using System.Collections;

public class TetrominoFactory : MonoBehaviour {

	enum State {
		UNDEFINED = 0,
		WAITING_FOR_SHAPE,
		CLEARING_ROW,
		SPAWN_SHAPE
	}

	public GameObject[] tetrominoPrefabs = new GameObject[7];

	State state;
	PreviewPoint preview;
	Tetromino current, next;

	void Awake () {
		preview = GetComponentInChildren<PreviewPoint> ();
	}

	public void PrepareNextLevel () {
		foreach (Tetromino old in FindObjectsOfType<Tetromino> ()) {
			Destroy (old.gameObject);	
		}
		state = State.SPAWN_SHAPE;
	}

	// Use this for initialization
	void Start () {
		state = State.SPAWN_SHAPE;
	}
	
	// Update is called once per frame
	void Update () {
		if (GameController.gameState != GameState.TETRIS) {
			return;
		}

		switch (state) {
		case State.SPAWN_SHAPE:
			SpawnNext ();
			break;
		case State.CLEARING_ROW:
			CheckForClear ();
			break;
		}
	}

	private void CheckForClear(){
		if (Grid.state == Grid.State.WAITING) {
			this.state = State.SPAWN_SHAPE;
		}
	}

	private void SpawnNext () {
		if (next == null) {
			next = SpawnRandomShape ();
			next.transform.position = preview.transform.position;

			SpawnRandomShape ().Activate (GameController.level);
		} else {
			next.Activate (GameController.level);

			next = SpawnRandomShape ();
			next.transform.position = preview.transform.position;
		}
		state = State.WAITING_FOR_SHAPE;
	}

	private Tetromino SpawnRandomShape () {
		Tetromino shape = Instantiate (tetrominoPrefabs [Random.Range (0, 7)]).GetComponent<Tetromino> ();
		shape.factory = this;
		return shape;
	}

	public void OnShapeDone () {
		Grid.DeleteFullRows ();
		state = State.CLEARING_ROW;
	}

}
