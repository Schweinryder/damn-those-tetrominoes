﻿using UnityEngine;
using System.Collections;

public class StartMenu : MonoBehaviour {

	static readonly Vector3 ShowPosition = new Vector3 (0, 0, -9);
	static readonly Vector3 HidePosition = new Vector3 (10000, 10000, -9);

	public void Hide () {
		transform.position = HidePosition;
	}

	public void Show () {
		transform.position = ShowPosition;
	}

}
