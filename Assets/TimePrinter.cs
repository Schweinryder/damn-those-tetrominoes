﻿using UnityEngine;
using System.Collections;

public class TimePrinter : MonoBehaviour {

	ValueText valueText;

	void Awake () {
		valueText = GetComponentInChildren<ValueText> ();
	}

	public void SetValue (string value) {
		valueText.SetValue (value);
	}

	void Update () {
		float countdown = GameController.RoundTime - GameController.TimeSinceRoundStart;
		SetValue (countdown.ToString ("#"));
	}

}
