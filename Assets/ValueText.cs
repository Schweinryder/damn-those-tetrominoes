﻿using UnityEngine;
using System.Collections;

public class ValueText : MonoBehaviour {

	TextMesh textMesh;

	// Use this for initialization
	void Awake () {
		textMesh = GetComponent<TextMesh> ();
	}
	
	public void SetValue (int value) {
		textMesh.text = value.ToString ();
	}

	public void SetValue (string value) {
		textMesh.text = value;
	}

}
